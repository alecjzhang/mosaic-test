// Based on:
//      http://nodeguide.com/beginner.html
//      http://www.tutorialspoint.com/nodejs/nodejs_express_framework.htm
//      https://isolasoftware.it/2012/05/28/call-rest-api-with-node-js/

var express = require('express');
var https = require('https')
var app = express();
var url = require('url');
var path    = require("path");

app.get('/callback', function (req, res) {
    var url_parts = url.parse(req.url, true);
    var path = url_parts.path
    var query = url_parts.query
    console.log(url_parts)
    if (query.code) {
        res.end("Authentication succeeded, authCode = " + query.code)
    }
    else {
        res.end("Authentication failed, url = " + path)
    }
})

app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname+'/build/index.html'));
})

app.get('/*.*s', function (req, res) {
    var url_parts = url.parse(req.url, true);
    var file = url_parts.path
    res.sendFile(path.join(__dirname+'/build/' + file));
})

var server = app.listen(8081, function () {

    var host = server.address().address
    var port = server.address().port

    console.log("Example app listening at http://%s:%s", host, port)

})